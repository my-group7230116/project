package data

import (
	"context"
	"time"

	"project_user_registration/database"
	"project_user_registration/model"
	"project_user_registration/utils"

	"github.com/lib/pq"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

type UserRepository interface {
	InsertUser(ctx context.Context, user model.User, requestID string) (database.User, error)
	Login(ctx context.Context, request model.LoginData, requestID string) (bool, model.UserDto, error)
}

type userRepository struct {
	queries  *database.Queries
	dbLogger *zap.Logger
}

func NewUserRepository(queries *database.Queries, dbLogger *zap.Logger) UserRepository {
	return &userRepository{
		queries:  queries,
		dbLogger: dbLogger,
	}
}

func (u userRepository) InsertUser(ctx context.Context, user model.User, requestID string) (database.User, error) {
	queries := u.queries

	insertedUser, err := queries.CreateUser(ctx, database.CreateUserParams{
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
	})

	if err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			if pqErr.Code == "23505" {
				errorResponse := utils.DuplicateEntry.Wrap(pqErr, "user already exists")
				errorResponse = errorResponse.WithProperty(utils.Statuscode, 409)
				u.dbLogger.Error("conflict error",
					zap.String("timestamp", time.Now().Format(time.RFC3339)),
					zap.String("layer", "databaseLayer"),
					zap.String("function", "InsertUser"),
					zap.String("requestID", requestID),
					zap.Any("requestBody", user),
					zap.Error(errorResponse),
					zap.Stack("stacktrace"),
				)
				return database.User{}, errorResponse
			}
		}

		errorResponse := utils.UnableToSave.Wrap(err, "failed to create user").WithProperty(utils.Statuscode, 500)
		u.dbLogger.Error("unable to create user",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "databaseLayer"),
			zap.String("function", "InsertUser"),
			zap.Any("requestBody", user),
			zap.String("requestID", requestID),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return database.User{}, errorResponse
	}

	return insertedUser, nil
}

func (u userRepository) Login(ctx context.Context, request model.LoginData, requestID string) (bool, model.UserDto, error) {
	queries := u.queries
	username := request.Username
	password := request.Password
	logger := u.dbLogger

	user, err := queries.GetUserByUsername(ctx, username)
	if err != nil {
		errorResponse := utils.InvalidCredintial.Wrap(err, "invalid username").WithProperty(utils.Statuscode, 400)
		logger.Error("incorrect username",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "databaseLayer"),
			zap.String("function", "Login"),
			zap.String("requestID", requestID),
			zap.String("username", username),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return false, model.UserDto{}, errorResponse
	}

	singleUser := model.UserDto{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
	}

	hashPassword := singleUser.Password
	match, err := matchPassword(password, hashPassword, requestID, logger)
	if err != nil {
		return match, model.UserDto{}, err
	}

	return match, singleUser, nil
}

func matchPassword(password string, hashPassword string, requestID string, logger *zap.Logger) (bool, error) {
	if err := bcrypt.CompareHashAndPassword([]byte(hashPassword), []byte(password)); err != nil {
		errorResponse := utils.InvalidCredintial.Wrap(err, "incorrect password").WithProperty(utils.Statuscode, 400)
		logger.Error("password mismatch",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "databaseLayer"),
			zap.String("function", "matchPassword"),
			zap.String("requestID", requestID),
			zap.String("password", password),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return false, errorResponse
	}

	return true, nil
}
