-- name: CreateUser :one
INSERT INTO
  users (username, email, password)
VALUES
  ($1, $2, $3) RETURNING *;

-- name: GetUserByUsername :one
SELECT
  id,
  username,
  email,
  password
FROM
  users
WHERE
  username = $1;