package service

import (
	"context"
	"net/http"
	"project_user_registration/model"
	"project_user_registration/utils"
	"time"

	"go.uber.org/zap"
)

func (u userService) LoginUser(ctx context.Context, request model.LoginData, requestID string) (model.Response, error) {
	logger := u.serviceLogger
	if err := request.Validate(); err != nil {
		errorResponse := utils.ValidationError.Wrap(err, "failed validation").WithProperty(utils.Statuscode, 400)
		logger.Error("validation error",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "LoginUser"),
			zap.String("requestID", requestID),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return model.Response{}, errorResponse
	}

	match, user, err := u.userRepo.Login(ctx, request, requestID)
	if err != nil || !match {
		return model.Response{}, err
	}

	userID := user.ID

	tokenPair, err := utils.GenerateTokenPair(ctx, userID, requestID, logger)
	if err != nil {
		return model.Response{}, err
	}

	loginResponse := model.Login{
		Username: user.Username,
		Tokens: model.TokenPair{
			AccessToken:  tokenPair["access_token"],
			RefreshToken: tokenPair["refresh_token"],
		},
	}

	response := model.Response{
		Data:    loginResponse,
		Status:  http.StatusOK,
		Message: "you're logged in successfully",
	}

	return response, nil
}
