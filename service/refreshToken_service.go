package service

import (
	"context"
	"net/http"
	"project_user_registration/model"
	"project_user_registration/utils"
	"time"

	"go.uber.org/zap"
)

func (u userService) RefreshToken(ctx context.Context, request model.RefreshToken, requestID string) (model.Response, error) {
	refreshToken := request.RefreshToken
	logger := u.serviceLogger
	if err := request.Validate(); err != nil {
		errorResponse := utils.ValidationError.Wrap(err, "failed validation").WithProperty(utils.Statuscode, 400)
		logger.Error("validation error",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "RefreshToken"),
			zap.String("requestID", requestID),
			zap.String("refrshToken", refreshToken),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return model.Response{}, errorResponse
	}

	id, err := utils.VerifyToken(ctx, refreshToken, requestID, logger)
	if err != nil {
		return model.Response{}, err
	}

	tokenPair, err := utils.GenerateTokenPair(ctx, id, requestID, logger)
	if err != nil {
		return model.Response{}, err
	}

	refreshtoken := model.TokenPair{
		AccessToken:  tokenPair["access_token"],
		RefreshToken: tokenPair["refresh_token"],
	}

	response := model.Response{
		Data:    refreshtoken,
		Status:  http.StatusOK,
		Message: "token refreshed successfully",
	}

	return response, nil
}
