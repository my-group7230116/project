package service

import (
	"context"
	"net/http"
	"project_user_registration/data"
	"project_user_registration/model"
	"project_user_registration/utils"
	"time"

	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

func hashPassword(password string, logger *zap.Logger, requestID string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		errorResponse := utils.InternalError.Wrap(err, "hashing error").WithProperty(utils.Statuscode, 500)
		logger.Error("hash_error",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "hashPassword"),
			zap.String("requestID", requestID),
			zap.String("password", password),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return "", errorResponse
	}

	hashPassword := string(hash)
	return hashPassword, nil
}

type UserService interface {
	RegisterUser(ctx context.Context, user model.User, requestID string) (model.Response, error)
	LoginUser(ctx context.Context, request model.LoginData, requestID string) (model.Response, error)
	RefreshToken(ctx context.Context, request model.RefreshToken, requestID string) (model.Response, error)
}

type userService struct {
	userRepo      data.UserRepository
	serviceLogger *zap.Logger
}

func NewUserService(userRepo data.UserRepository, seviceLogger *zap.Logger) UserService {
	return &userService{
		userRepo:      userRepo,
		serviceLogger: seviceLogger,
	}
}

func (u userService) RegisterUser(ctx context.Context, user model.User, requestID string) (model.Response, error) {
	password := user.Password

	select {
	case <-ctx.Done():
		return model.Response{}, nil
	default:
	}

	if err := user.Validate(); err != nil {
		errorResponse := utils.ValidationError.Wrap(err, "failed validation")
		errorResponse = errorResponse.WithProperty(utils.Statuscode, 400)
		u.serviceLogger.Error("validation error",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "RegisterUser"),
			zap.String("requestID", requestID),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return model.Response{}, errorResponse
	}

	hashPassword, errorResponse := hashPassword(password, u.serviceLogger, requestID)
	if errorResponse != nil {
		return model.Response{}, errorResponse
	}

	user.Password = hashPassword

	select {
	case <-ctx.Done():
		return model.Response{}, nil
	default:
	}

	insertedUser, errorResponse := u.userRepo.InsertUser(ctx, user, requestID)
	if errorResponse != nil {
		return model.Response{}, errorResponse
	}

	response := model.Response{
		Data:    insertedUser,
		Status:  http.StatusCreated,
		Message: "Congratulations! 🎉 You have successfully registered",
	}

	u.serviceLogger.Info("successfull registration",
		zap.String("timestamp", time.Now().Format(time.RFC3339)),
		zap.Int64("inserted_userID", insertedUser.ID),
	)

	return response, nil
}
