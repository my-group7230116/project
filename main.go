package main

import (
	"errors"
	"log"
	"os"
	"project_user_registration/config"
	"project_user_registration/data"
	"project_user_registration/database"
	"project_user_registration/handler"
	"project_user_registration/middleware"
	"project_user_registration/service"
	"project_user_registration/utils"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"go.uber.org/zap"
)

//	@title			User Registration API
//	@version		1.0
//	@description	This is a sample server for user registration platform.

//	@contact.name	Engdawork yismaw
//	@contact.email	engdaworkyismaw9@gmail.com

// @host		localhost:9000
// @schemes	http
func main() {

	appLogger := utils.InitLogger()
	defer appLogger.GetLogger().Sync()

	if err := godotenv.Load(); err != nil {
		errorResponse := utils.InternalError.Wrap(err, "error loading .env file")
		appLogger.GetLogger().Error("failed to load .env file",
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		log.Fatal(errorResponse)
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	dbURL := os.Getenv("DB_URL")
	if dbURL == "" {
		err := errors.New("unable to get dbURL")
		errorResponse := utils.InternalError.Wrap(err, "failed to get dbURL in .env file")
		appLogger.GetLogger().Error("failed to get dbURL",
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		log.Fatal(errorResponse)
	}
	databaseLogger := appLogger.GetLogger().Named("databaseLogger")
	serviceLogger := appLogger.GetLogger().Named("serviceLogger")
	handlerLogger := appLogger.GetLogger().Named("handlerLogger")
	middlewareLogger := appLogger.GetLogger().Named("middlewareLogger")

	if err := utils.InitRedis(serviceLogger); err != nil {
		log.Fatal(err)
	}

	timeoutMiddleware := middleware.TimeoutMiddleware
	errorMiddleware := middleware.ErrorMiddleware
	requestIdMiddleware := middleware.RequestIDMiddleware
	loggerMiddleware := middleware.LoggerMiddleware

	instance := gin.Default()
	instance.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	instance.Use(requestIdMiddleware())
	instance.Use(loggerMiddleware(middlewareLogger))
	instance.Use(timeoutMiddleware(middlewareLogger))
	instance.Use(errorMiddleware())

	db, err := config.DbConnection(databaseLogger, dbURL)
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	queries := database.New(db)

	userRepo := data.NewUserRepository(queries, databaseLogger)
	userService := service.NewUserService(userRepo, serviceLogger)
	userController := handler.NewUserHandler(instance, userService, handlerLogger)
	userController.InitRouter()

	instance.Run(":" + port)
}
