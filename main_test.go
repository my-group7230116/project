package main

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"project_user_registration/data"
	"project_user_registration/database"
	"project_user_registration/handler"
	"project_user_registration/middleware"
	"project_user_registration/model"
	"project_user_registration/service"
	"project_user_registration/test"
	"project_user_registration/utils"
	"strings"
	"testing"
	"time"

	"github.com/DATA-DOG/go-txdb"
	"github.com/cucumber/godog"
	"github.com/cucumber/godog/colors"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func init() {
	// Registering txdb as a driver with a unique name
	txdb.Register("txdb", "postgres", "postgres://postgres:engdawork9397@localhost:5432/project?sslmode=disable")
}

type GodogTests struct {
	DB                 *sql.DB
	userRepo           data.UserRepository
	userService        service.UserService
	userHandler        *handler.UserHandler
	server             *httptest.Server
	response           *http.Response
	Engine             *gin.Engine
	registeredUsername string
	userData           []byte
}

// Register step function
func (g *GodogTests) aUserIsRegisteredWithValidCredentials() error {
	user := model.User{
		Username: "newuser",
		Email:    "newuser@gmail.com",
		Password: "Newuser@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldRegisterMeSuccessfully() error {
	if g.response.StatusCode != http.StatusCreated {
		return fmt.Errorf("expected status code 201, got %d", g.response.StatusCode)
	}

	return nil
}

func (g *GodogTests) aUserWithTheUsernameIsAlreadyRegistered(username string) error {
	user := model.User{
		Username: username,
		Email:    "testuser@gmail.com",
		Password: "Testuser@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	g.registeredUsername = username

	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("expected status code 201, but got %d", resp.StatusCode)
	}

	g.response = resp

	return nil
}

func (g *GodogTests) iAttemptToRegisterWithTheSameUsername() error {
	user := model.User{
		Username: g.registeredUsername,
		Email:    "duplicate@gmail.com",
		Password: "Duplicate@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameAlreadyExists() error {
	var jsonResp map[string]string
	if err := json.NewDecoder(g.response.Body).Decode(&jsonResp); err != nil {
		return err
	}

	got := jsonResp["error"]
	expected := "user already exists"

	if g.response.StatusCode != http.StatusConflict {
		return fmt.Errorf("expected status code 409, but got %d", g.response.StatusCode)
	}

	if !strings.Contains(got, expected) {
		return fmt.Errorf("expected error message %s, but got %s", expected, got)
	}

	return nil
}

func (g *GodogTests) iAmRegisteringWithAnInvalidEmailFormat() error {
	user := model.User{
		Username: "testuser",
		Email:    "invalidemailformat.",
		Password: "Invalid@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	g.userData = jsonData

	return nil
}

func (g *GodogTests) iSubmitTheRegistrationForm() error {
	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(g.userData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldReturnAnErrorMessageIndicatingThatTheEmailFormatIsInvalid() error {
	var jsonResp map[string]string
	if err := json.NewDecoder(g.response.Body).Decode(&jsonResp); err != nil {
		return err
	}

	if g.response.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("expected status code 400, but got %d", g.response.StatusCode)
	}

	got := jsonResp["error"]
	expected := "must be a valid email address"

	if !strings.Contains(got, expected) {
		return fmt.Errorf("expected error message %s, but got %s", expected, got)
	}

	return nil
}

func (g *GodogTests) iAmRegisteringWithAWeakPassword() error {
	user := model.User{
		Username: "testuser",
		Email:    "testuser@gmail.com",
		Password: "12345678",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	g.userData = jsonData

	return nil
}

func (g *GodogTests) iSubmitTheWeakPasswordForm() error {
	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(g.userData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordIsNotStrong() error {
	var jsonResp map[string]string
	if err := json.NewDecoder(g.response.Body).Decode(&jsonResp); err != nil {
		return err
	}

	if g.response.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("expected status code 400, but got %d", g.response.StatusCode)
	}

	got := jsonResp["error"]
	expected := "ApplicationError.VALIDATION_ERROR: failed validation"

	if !strings.Contains(got, expected) {
		return fmt.Errorf("expected error message %s, but got %s", expected, got)
	}

	return nil
}

func (g *GodogTests) iAmRegisteringWithaUsernameLessThanFiveCharactersLong() error {
	user := model.User{
		Username: "test",
		Email:    "testuser@gmail.com",
		Password: "Testuser@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	g.userData = jsonData

	return nil
}

func (g *GodogTests) iSubmitTheInvalidUsernameForm() error {
	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(g.userData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameMustBeAtLeastFiveCharLong() error {
	var jsonResp map[string]string
	if err := json.NewDecoder(g.response.Body).Decode(&jsonResp); err != nil {
		return err
	}

	if g.response.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("expected status code 400, but got %d", g.response.StatusCode)
	}

	got := jsonResp["error"]
	expected := "the length must be no less than 5"

	if !strings.Contains(got, expected) {
		return fmt.Errorf("expected error message %s, but got %s", expected, got)
	}

	return nil
}

func (g *GodogTests) iAmRegisteringWithaPasswordThatDoesNotMeetRequirement() error {
	user := model.User{
		Username: "testuser",
		Email:    "testuser@gmail.com",
		Password: "weakpassword",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	g.userData = jsonData

	return nil
}

func (g *GodogTests) iSubmitInvalidPasswordForm() error {
	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(g.userData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordRequirement() error {
	var jsonResp map[string]string
	if err := json.NewDecoder(g.response.Body).Decode(&jsonResp); err != nil {
		return err
	}

	if g.response.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("expected status code 400, but got %d", g.response.StatusCode)
	}

	got := jsonResp["error"]
	expected := "password must contain at least one uppercase letter"

	if !strings.Contains(got, expected) {
		return fmt.Errorf("expected error message %s, but got %s", expected, got)
	}

	return nil
}

// Login step function
func (g *GodogTests) iAmRegisteredUserWithValidCredentials() error {
	user := model.User{
		Username: "loginuser",
		Email:    "loginuser@gmail.com",
		Password: "Loginuser@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	resp, err := g.server.Client().Post(g.server.URL+"/user/register", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	g.response = resp

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("expected status code 201, but got %d", resp.StatusCode)
	}

	return nil
}

func (g *GodogTests) iLogInWithMyUsernameAndPassword() error {
	user := model.LoginData{
		Username: "loginuser",
		Password: "Loginuser@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	resp, err := g.server.Client().Post(g.server.URL+"/user/login", "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("expected status code 200, but got %d", resp.StatusCode)
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldGenerateAJwtAccessTokenAndRefreshToken() error {
	var jsonBody model.LoginResponse
	if err := json.NewDecoder(g.response.Body).Decode(&jsonBody); err != nil {
		return err
	}

	accessToken := jsonBody.Data.TokenPair.AccessToken
	refreshToken := jsonBody.Data.TokenPair.RefreshToken

	if accessToken == "" || refreshToken == "" {
		return fmt.Errorf("expected to get accessToken and refreshToken, but got accessToken %s and refreshToken %s", accessToken, refreshToken)
	}

	return nil
}

func (g *GodogTests) iAmAttemptingToLogInWithAnInvalidUsername() error {
	if err := g.iAmRegisteredUserWithValidCredentials(); err != nil {
		return err
	}

	user := model.LoginData{
		Username: "invalidUsername",
		Password: "Loginuser@10",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	g.userData = jsonData

	return nil
}

func (g *GodogTests) iSubmitTheInvalidUsernameLoginForm() error {
	resp, err := g.server.Client().Post(g.server.URL+"/user/login", "application/json", bytes.NewBuffer(g.userData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameIsNotRegistered() error {
	var jsonResp map[string]string
	if err := json.NewDecoder(g.response.Body).Decode(&jsonResp); err != nil {
		return err
	}

	if g.response.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("expected status code 400, but got %d", g.response.StatusCode)
	}

	got := jsonResp["error"]
	expected := "invalid username"

	if !strings.Contains(got, expected) {
		return fmt.Errorf("expected error message %s, but got %s", expected, got)
	}

	return nil
}

func (g *GodogTests) iAmAttemptingToLogInWithAnInvalidPassword() error {
	if err := g.iAmRegisteredUserWithValidCredentials(); err != nil {
		return err
	}

	user := model.LoginData{
		Username: "loginuser",
		Password: "Loginuser@7",
	}

	jsonData, err := test.MarshalData(user)
	if err != nil {
		return err
	}

	g.userData = jsonData

	return nil
}

func (g *GodogTests) iSubmitTheInvalidPasswordLoginForm() error {
	resp, err := g.server.Client().Post(g.server.URL+"/user/login", "application/json", bytes.NewBuffer(g.userData))
	if err != nil {
		return err
	}

	g.response = resp

	return nil
}

func (g *GodogTests) theSystemShouldReturnAnErrorMessageIndicatingThatThePassswordIsIncorrect() error {
	var jsonResp map[string]string
	if err := json.NewDecoder(g.response.Body).Decode(&jsonResp); err != nil {
		return err
	}

	if g.response.StatusCode != http.StatusBadRequest {
		return fmt.Errorf("expected status code 400, but got %d", g.response.StatusCode)
	}

	got := jsonResp["error"]
	expected := "incorrect password"

	if !strings.Contains(got, expected) {
		return fmt.Errorf("expected error message %s, but got %s", expected, got)
	}

	return nil
}

func InitializeScenario(g *GodogTests) func(s *godog.ScenarioContext) {
	return func(s *godog.ScenarioContext) {
		appLogger := utils.InitLogger()
		defer appLogger.GetLogger().Sync()
		dbLogger := appLogger.GetLogger().Named("databaseLogger")
		handlerLogger := appLogger.GetLogger().Named("handlerLogger")
		serviceLogger := appLogger.GetLogger().Named("serviceLogger")
		middlewareLogger := appLogger.GetLogger().Named("middlewareLogger")

		s.Before(func(ctx context.Context, _ *godog.Scenario) (context.Context, error) {
			// Establish a new transaction using txdb
			db, err := sql.Open("txdb", fmt.Sprintf("txdb_%d", time.Now().UnixNano()))
			if err != nil {
				return ctx, err
			}

			// Set up the Gin engine and other dependencies
			timeoutMiddleware := middleware.TimeoutMiddleware
			errorMiddleware := middleware.ErrorMiddleware
			requestIdMiddleware := middleware.RequestIDMiddleware
			loggerMiddleware := middleware.LoggerMiddleware

			if err := utils.InitRedis(serviceLogger); err != nil {
				log.Fatal(err)
			}

			engine := gin.Default()
			engine.Use(requestIdMiddleware())
			engine.Use(loggerMiddleware(middlewareLogger))
			engine.Use(timeoutMiddleware(middlewareLogger))
			engine.Use(errorMiddleware())
			queries := database.New(db)
			g.DB = db
			g.userRepo = data.NewUserRepository(queries, dbLogger)
			g.userService = service.NewUserService(g.userRepo, serviceLogger)
			g.Engine = engine
			g.userHandler = handler.NewUserHandler(engine, g.userService, handlerLogger)
			g.userHandler.InitRouter()

			g.server = httptest.NewServer(engine)

			return ctx, nil
		})

		s.After(func(ctx context.Context, _ *godog.Scenario, err error) (context.Context, error) {
			// Rollback the transaction by closing the DB connection
			if err := g.DB.Close(); err != nil {
				return ctx, err
			}
			if g.response != nil {
				g.response.Body.Close()
			}
			g.server.Close()
			g.registeredUsername = ""
			g.response = nil
			g.userData = nil
			return ctx, nil
		})

		// Register step definitions
		s.Step(`^I am registering with valid credentials$`, g.aUserIsRegisteredWithValidCredentials)
		s.Step(`^the system should register me successfully$`, g.theSystemShouldRegisterMeSuccessfully)

		s.Step(`^a user with the username "([^"]*)" is already registered$`, g.aUserWithTheUsernameIsAlreadyRegistered)
		s.Step(`^I attempt to register with the same username$`, g.iAttemptToRegisterWithTheSameUsername)
		s.Step(`^the system should return an error message indicating that the username already exists$`, g.theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameAlreadyExists)

		s.Step(`^I am registering with an invalid email format$`, g.iAmRegisteringWithAnInvalidEmailFormat)
		s.Step(`^I submit the registration form$`, g.iSubmitTheRegistrationForm)
		s.Step(`^the system should return an error message indicating that the email format is invalid$`, g.theSystemShouldReturnAnErrorMessageIndicatingThatTheEmailFormatIsInvalid)

		s.Step(`^I am registering with a weak password$`, g.iAmRegisteringWithAWeakPassword)
		s.Step(`^I submit the registration form$`, g.iSubmitTheWeakPasswordForm)
		s.Step(`^the system should return an error message indicating that the password is not strong enough$`, g.theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordIsNotStrong)

		s.Step(`^I am registering with a username less than 5 characters long$`, g.iAmRegisteringWithaUsernameLessThanFiveCharactersLong)
		s.Step(`^I submit the registration form$`, g.iSubmitTheInvalidUsernameForm)
		s.Step(`^the system should return an error message indicating that the username must be at least 5 characters long$`, g.theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameMustBeAtLeastFiveCharLong)

		s.Step(`^I am registering with a password that does not meet the strength requirements$`, g.iAmRegisteringWithaPasswordThatDoesNotMeetRequirement)
		s.Step(`^I submit the registration form$`, g.iSubmitInvalidPasswordForm)
		s.Step(`^the system should return an error message indicating the password requirements$`, g.theSystemShouldReturnAnErrorMessageIndicatingThatThePasswordRequirement)

		// Login step definitions
		s.Step(`^I am a registered user with valid credentials$`, g.iAmRegisteredUserWithValidCredentials)
		s.Step(`^I log in with my username and password$`, g.iLogInWithMyUsernameAndPassword)
		s.Step(`^the system should generate a JWT token for authentication and issue a refresh token$`, g.theSystemShouldGenerateAJwtAccessTokenAndRefreshToken)

		s.Step(`^I am attempting to log in with an invalid username$`, g.iAmAttemptingToLogInWithAnInvalidUsername)
		s.Step(`^I submit the login form$`, g.iSubmitTheInvalidUsernameLoginForm)
		s.Step(`^the system should return an error message indicating that the username is not registered$`, g.theSystemShouldReturnAnErrorMessageIndicatingThatTheUsernameIsNotRegistered)

		s.Step(`^I am attempting to log in with an invalid password$`, g.iAmAttemptingToLogInWithAnInvalidPassword)
		s.Step(`^I submit the login form$`, g.iSubmitTheInvalidPasswordLoginForm)
		s.Step(`^the system should return an error message indicating that the password is incorrect$`, g.theSystemShouldReturnAnErrorMessageIndicatingThatThePassswordIsIncorrect)

	}
}

func TestMain(m *testing.M) {
	opts := godog.Options{
		Output: colors.Colored(os.Stdout),
		Format: "pretty",
		Paths:  []string{"features"},
	}

	status := godog.TestSuite{
		Name:                "godogs",
		ScenarioInitializer: InitializeScenario(&GodogTests{}),
		Options:             &opts,
	}.Run()

	os.Exit(status)
}
