package handler

import (
	"errors"
	"project_user_registration/model"
	"project_user_registration/service"
	"project_user_registration/utils"
	"time"

	_ "project_user_registration/docs"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

type UserHandler struct {
	engine        *gin.Engine
	userService   service.UserService
	handlerLogger *zap.Logger
}

func NewUserHandler(engine *gin.Engine, userService service.UserService, handlerLogger *zap.Logger) *UserHandler {
	return &UserHandler{
		engine:        engine,
		userService:   userService,
		handlerLogger: handlerLogger,
	}
}

func (u *UserHandler) InitRouter() {
	r := u.engine
	api := r.Group("/user")

	api.POST("/register", u.RegisterHandler)
	api.POST("/login", u.loginHandler)
	api.POST("/token", u.refrehsTokenHandler)
}

// RegisterHandler  godoc
// @Summary		    create user
// @Description	    Insert a new user
// @Tags			user
// @ID				create-user
// @Accept			json
// @Produce		    json
// @Param			user	body		model.User	true	"User Data"
// @Success		    201		{object}	model.Response
// @Router			/user/register [post]
func (u *UserHandler) RegisterHandler(c *gin.Context) {
	ctx := c.Request.Context()

	select {
	case <-ctx.Done():
		return
	default:
	}

	reqId, exist := c.Get("requestID")
	if !exist {
		err := errors.New("unable to get requestID from the gin context")
		errorResponse := utils.InternalError.Wrap(err, "failed to get request id").WithProperty(utils.Statuscode, 500)
		c.Error(errorResponse)
		u.handlerLogger.Error("requestID is not exist in the gin context",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "RegisterHandler"),
			zap.String("context_key", "requestID"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return
	}

	requestID, ok := reqId.(string)
	if !ok {
		err := errors.New("unable to convert type any to string")
		errorResponse := utils.InternalError.Wrap(err, "failed to convert requestId type any to string").WithProperty(utils.Statuscode, 500)
		c.Error(errorResponse)
		u.handlerLogger.Error("requestId is not exist in the context",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "RegisterHandler"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return
	}

	var user model.User
	if err := c.ShouldBindJSON(&user); err != nil {
		errorResponse := utils.InvalidRequest.Wrap(err, "failed to decode json request body").WithProperty(utils.Statuscode, 400)
		c.Error(errorResponse)
		u.handlerLogger.Error("bad request",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "RegisterHandler"),
			zap.String("requestID", requestID),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return
	}

	select {
	case <-ctx.Done():
		return
	default:
	}

	resp, err := u.userService.RegisterUser(ctx, user, requestID)
	if err != nil {
		c.Error(err)
		return
	}

	u.handlerLogger.Info("successful registration",
		zap.String("timestamp", time.Now().Format(time.RFC3339)),
		zap.Any("response", resp.Data),
	)

	c.JSON(resp.Status, resp)
}

// loginHandler  godoc
// @Summary		    login user
// @Description	    authentication
// @Tags			user
// @ID				login-user
// @Accept			json
// @Produce		    json
// @Param			user	body		model.LoginData	true	"Login Data"
// @Success		    201		{object}	model.Response
// @Router			/user/login [post]
func (u UserHandler) loginHandler(c *gin.Context) {
	logger := u.handlerLogger
	ctx := c.Request.Context()

	reqId, exist := c.Get("requestID")
	if !exist {
		err := errors.New("unable to get requestID from the gin context")
		errorResponse := utils.InternalError.Wrap(err, "failed to get request id")
		errorResponse = errorResponse.WithProperty(utils.Statuscode, 500)
		c.Error(errorResponse)
		u.handlerLogger.Error("requestID is not exist in the gin context",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "loginHandler"),
			zap.String("context_key", "requestID"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return
	}

	requestID, ok := reqId.(string)
	if !ok {
		err := errors.New("unable to convert type any to string")
		errorResponse := utils.InternalError.Wrap(err, "failed to convert requestId type any to string")
		errorResponse = errorResponse.WithProperty(utils.Statuscode, 500)
		c.Error(errorResponse)
		u.handlerLogger.Error("requestId is not exist in the context",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "loginHandler"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return
	}

	var request model.LoginData
	if err := c.ShouldBindJSON(&request); err != nil {
		errorResponse := utils.InvalidRequest.Wrap(err, "failed to bind json request body")
		errorResponse = errorResponse.WithProperty(utils.Statuscode, 400)
		logger.Error("uable to bind json request body",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "loginHandler"),
			zap.String("requestID", requestID),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return
	}

	response, err := u.userService.LoginUser(ctx, request, requestID)
	if err != nil {
		c.Error(err)
		return
	}

	logger.Info("user loggedIn successfully",
		zap.String("timestamp", time.Now().Format(time.RFC3339)),
		zap.Any("response", response.Data),
	)

	c.JSON(response.Status, response)
}

// refrehsTokenHandler  godoc
// @Summary		    refresh token
// @Description	    to get new token
// @Tags			user
// @ID				refresh-token
// @Accept			json
// @Produce		    json
// @Param			token	body		model.RefreshToken	true	"Refresh Token"
// @Success		    201		{object}	model.Response
// @Router			/user/token [post]
func (u UserHandler) refrehsTokenHandler(c *gin.Context) {
	ctx := c.Request.Context()
	logger := u.handlerLogger
	var request model.RefreshToken

	reqId, exist := c.Get("requestID")
	if !exist {
		err := errors.New("unable to get requestID from the gin context")
		errorResponse := utils.InternalError.Wrap(err, "failed to get request id")
		errorResponse = errorResponse.WithProperty(utils.Statuscode, 500)
		c.Error(errorResponse)
		u.handlerLogger.Error("requestID is not exist in the gin context",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "refrehsTokenHandler"),
			zap.String("context_key", "requestID"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return
	}

	requestID, ok := reqId.(string)
	if !ok {
		err := errors.New("unable to convert type any to string")
		errorResponse := utils.InternalError.Wrap(err, "failed to convert requestId type any to string")
		errorResponse = errorResponse.WithProperty(utils.Statuscode, 500)
		c.Error(errorResponse)
		u.handlerLogger.Error("requestId is not exist in the context",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "refrehsTokenHandler"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		errorResponse := utils.ValidationError.Wrap(err, "failed validation").WithProperty(utils.Statuscode, 400)
		logger.Error("validation error",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "handlerLayer"),
			zap.String("function", "refreshTokenHandler"),
			zap.String("requestID", requestID),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return
	}

	response, err := u.userService.RefreshToken(ctx, request, requestID)
	if err != nil {
		c.Error(err)
		return
	}

	logger.Info("successful refresh token",
		zap.String("timestamp", time.Now().Format(time.RFC3339)),
		zap.Any("response", response.Data),
	)

	c.JSON(response.Status, response)
}
