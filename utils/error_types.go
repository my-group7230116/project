package utils

import "github.com/joomcode/errorx"

var (
	DatabaseErrorNamespace       = errorx.NewNamespace("DatabaseError")
	ApplicationErrorNamespace    = errorx.NewNamespace("ApplicationError")
	AuthenticationErrorNamespace = errorx.NewNamespace("AuthenticationError")
)

var (
	DuplicateEntry  = DatabaseErrorNamespace.NewType("DUPLICATE_ENTRY")
	UnableToSave    = DatabaseErrorNamespace.NewType("UNABLE_TO_SAVE")
	ConnectionError = DatabaseErrorNamespace.NewType("CONNECTION_ERROR")
)

var (
	ValidationError    = ApplicationErrorNamespace.NewType("VALIDATION_ERROR")
	InvalidRequest     = ApplicationErrorNamespace.NewType("INVALID_REQUEST")
	InternalError      = ApplicationErrorNamespace.NewType("INTERNAL_ERROR")
	TimeoutError       = ApplicationErrorNamespace.NewType("TIMEOUT_ERROR")
	AppConnectionError = ApplicationErrorNamespace.NewType("CONNECTION_ERROR")
)

var (
	InvalidCredintial = AuthenticationErrorNamespace.NewType("INVALID_CREDINTIAL")
	AuthInternalError = AuthenticationErrorNamespace.NewType("INTERNAL_ERROR")
	Unauthorized      = AuthenticationErrorNamespace.NewType("UNAUTHORIZED")
)

var (
	Statuscode = errorx.RegisterProperty("status")
)
