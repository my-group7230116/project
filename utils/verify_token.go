package utils

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/joho/godotenv"
	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
)

func VerifyToken(ctx context.Context, tokenString string, requestId string, logger *zap.Logger) (int64, error) {
	used, err := Client.Get(ctx, tokenString).Result()
	if err == redis.Nil || used == "true" {
		errorResponse := Unauthorized.Wrap(err, "invalid or expired refresh token").WithProperty(Statuscode, 401)
		logger.Error("refresh token is already used or expired",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "VerifyToken"),
			zap.String("requestID", requestId),
			zap.String("used", used),
			zap.String("refreshToken", tokenString),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return 0, errorResponse
	} else if err != nil {
		errorResponse := InternalError.Wrap(err, "Failed to query refresh token").WithProperty(Statuscode, 500)
		logger.Error("failed to get refresh token in redis",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "VerifyToken"),
			zap.String("requestID", requestId),
			zap.String("refreshToken", tokenString),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return 0, errorResponse
	}

	if err = Client.Set(ctx, tokenString, "true", 0).Err(); err != nil {
		errorResponse := InternalError.Wrap(err, "failed to update refresh token").WithProperty(Statuscode, 500)
		logger.Error("failed to update the refresh token to be used",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "VerifyToken"),
			zap.String("requestID", requestId),
			zap.String("refreshToken", tokenString),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return 0, errorResponse
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			err := fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			errorResponse := InternalError.Wrap(err, "incorrect signing method").WithProperty(Statuscode, 500)
			logger.Error("incorrect signing method",
				zap.String("timestamp", time.Now().Format(time.RFC3339)),
				zap.String("layer", "serviceLayer"),
				zap.String("function", "VerifyToken"),
				zap.String("request_id", requestId),
				zap.Any("signing_method", token.Header["alg"]),
				zap.Error(errorResponse),
				zap.Stack("stacktrace"),
			)
			return nil, err
		}

		if err := godotenv.Load(".env"); err != nil {
			errorResponse := InternalError.Wrap(err, "failed to load .env file").WithProperty(Statuscode, 500)
			logger.Error("error to load .env file",
				zap.String("timestamp", time.Now().Format(time.RFC3339)),
				zap.String("layer", "serviceLayer"),
				zap.String("function", "GenerateTokenPair"),
				zap.String("requestID", requestId),
				zap.Error(errorResponse),
				zap.Stack("stacktrace"),
			)

			return nil, errorResponse
		}

		secret := os.Getenv("JWT_SECRET")
		secretKey := []byte(secret)

		return secretKey, nil
	})

	if err != nil {
		errorResponse := Unauthorized.Wrap(err, "invalid token").WithProperty(Statuscode, 401)
		logger.Error("invlaid token",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "VerifyToken"),
			zap.String("request_id", requestId),
			zap.String("token", tokenString),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return 0, errorResponse
	}

	if !token.Valid {
		err := fmt.Errorf("token provided is invalid or has expired")
		errorResponse := Unauthorized.Wrap(err, "invalid token").WithProperty(Statuscode, 401)
		logger.Error("invlaid token",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "VerifyToken"),
			zap.String("request_id", requestId),
			zap.String("token", tokenString),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return 0, errorResponse
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		err := fmt.Errorf("unable to extract claims from the refresh token")
		errorResponse := Unauthorized.Wrap(err, "failed to get claims").WithProperty(Statuscode, 401)
		logger.Error("unable to get claims",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "VerifyToken"),
			zap.String("request_id", requestId),
			zap.String("token", tokenString),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return 0, errorResponse
	}

	idFloat, ok := claims["id"].(float64)
	if !ok {
		err := fmt.Errorf("unable to extract the id from claims")
		errorResponse := Unauthorized.Wrap(err, "failed to get user id").WithProperty(Statuscode, 401)
		logger.Error("unable to get user id",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "VerifyToken"),
			zap.String("request_id", requestId),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return 0, errorResponse
	}

	id := int64(idFloat)

	return id, nil
}
