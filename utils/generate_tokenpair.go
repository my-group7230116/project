package utils

import (
	"context"
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/joho/godotenv"
	"go.uber.org/zap"
)

func GenerateTokenPair(ctx context.Context, id int64, requestID string, logger *zap.Logger) (map[string]string, error) {
	if err := godotenv.Load(".env"); err != nil {
		errorResponse := InternalError.Wrap(err, "failed to load .env file").WithProperty(Statuscode, 500)
		logger.Error("error to load .env file",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "GenerateTokenPair"),
			zap.String("requestID", requestID),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return nil, errorResponse
	}

	secret := os.Getenv("JWT_SECRET")
	secretKey := []byte(secret)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"id":  id,
			"exp": time.Now().Add(15 * time.Minute).Unix(),
		},
	)

	accessTokenString, err := token.SignedString(secretKey)
	if err != nil {
		errorResponse := AuthInternalError.Wrap(err, "failed to generate access token").WithProperty(Statuscode, 500)
		logger.Error("unable to generate accessToken",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "GenerateTokenPair"),
			zap.String("requestID", requestID),
			zap.Int64("userID", id),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return nil, errorResponse
	}

	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"id":  id,
			"exp": time.Now().Add(30 * 24 * time.Hour).Unix(),
		},
	)

	refreshTokenString, err := refreshToken.SignedString(secretKey)
	if err != nil {
		errorResponse := AuthInternalError.Wrap(err, "failed to generate refresh token").WithProperty(Statuscode, 500)
		logger.Error("unable to generate refreshToken",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "GenerateTokenPair"),
			zap.String("requestID", requestID),
			zap.Int64("userID", id),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)

		return nil, errorResponse
	}

	refreshTokenExpiration := 30 * 24 * time.Hour
	if err = Client.Set(ctx, refreshTokenString, "false", refreshTokenExpiration).Err(); err != nil {
		errorResponse := InternalError.Wrap(err, "failed to store refresh token in redis").WithProperty(Statuscode, 500)
		logger.Error("unable to store refresh token in redis",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "GenerateTokenPair"),
			zap.String("requestID", requestID),
			zap.String("refreshToken", refreshTokenString),
			zap.Int64("userID", id),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return nil, errorResponse
	}

	tokenPair := map[string]string{
		"access_token":  accessTokenString,
		"refresh_token": refreshTokenString,
	}

	return tokenPair, nil
}
