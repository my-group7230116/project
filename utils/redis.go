package utils

import (
	"context"
	"time"

	"github.com/redis/go-redis/v9"
	"go.uber.org/zap"
)

var Client *redis.Client
var ctx = context.Background()

func InitRedis(serviceLogger *zap.Logger) error {
	Client = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	if err := Client.Ping(ctx).Err(); err != nil {
		errorResponse := AppConnectionError.Wrap(err, "failed to connect to Redis")
		serviceLogger.Error("connection error",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "serviceLayer"),
			zap.String("function", "InitRedis"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return errorResponse
	}

	return nil
}
