package middleware

import (
	"errors"
	"net/http"
	"project_user_registration/utils"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func LoggerMiddleware(requestIdLogger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		id, exist := c.Get("requestID")
		if !exist {
			err := errors.New("requestID not found in gin context")
			errorResponse := utils.InternalError.Wrap(err, "failed to get requestID")
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": errorResponse.Error()})
			requestIdLogger.Error("requestID error",
				zap.String("timestamp", time.Now().Format(time.RFC3339)),
				zap.String("layer", "middlewareLayer"),
				zap.String("function", "LoggerMiddleware"),
				zap.Error(errorResponse),
				zap.Stack("stacktrace"),
			)
			return
		}

		requestID := id.(string)

		c.Next()

		requestIdLogger.Info("request processed",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("requestID", requestID),
			zap.String("method", c.Request.Method),
			zap.String("path", c.Request.URL.Path),
			zap.Int("statusCode", c.Writer.Status()),
			zap.String("clientIP", c.ClientIP()),
		)

	}
}
