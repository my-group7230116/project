package model

import (
	"project_user_registration/utils"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

type User struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u User) Validate() error {
	return validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Required, validation.Length(5, 0), is.Alphanumeric),
		validation.Field(&u.Email, validation.Required, is.Email),
		validation.Field(&u.Password, validation.Required, validation.Length(8, 0), validation.By(utils.ValidatePassword)),
	)
}

type LoginData struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (l LoginData) Validate() error {
	return validation.ValidateStruct(&l,
		validation.Field(&l.Username, validation.Required, validation.Length(5, 0), is.Alphanumeric),
		validation.Field(&l.Password, validation.Required, validation.Length(8, 0), validation.By(utils.ValidatePassword)),
	)
}

type RefreshToken struct {
	RefreshToken string `json:"refresh_token"`
}

func (r RefreshToken) Validate() error {
	return validation.ValidateStruct(&r,
		validation.Field(&r.RefreshToken, validation.Required),
	)
}

type LoginResponse struct {
	Data struct {
		Username  string    `json:"username"`
		TokenPair TokenPair `json:"token_pair"`
	} `json:"data"`
	Status  int    `json:"status"`
	Message string `json:"message"`
}
