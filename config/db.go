package config

import (
	"database/sql"
	"project_user_registration/utils"
	"time"

	_ "github.com/lib/pq"
	"go.uber.org/zap"
)

func DbConnection(databaseLogger *zap.Logger, dbURL string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dbURL)
	if err != nil {
		errorResponse := utils.ConnectionError.Wrap(err, "unable to connect postgres database").WithProperty(utils.Statuscode, 500)
		databaseLogger.Error("db connection error",
			zap.String("timestamp", time.Now().Format(time.RFC3339)),
			zap.String("layer", "databaseLayer"),
			zap.String("function", "DbConnection"),
			zap.Error(errorResponse),
			zap.Stack("stacktrace"),
		)
		return nil, errorResponse
	}

	return db, nil
}
